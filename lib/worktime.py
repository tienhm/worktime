import requests

from lib.captcha import Captcha


class WorkTime(object):
    """Gateway to hack worktime"""

    def __init__(self, email, password):
        self.email = email
        self.password = password
        self.cookies = None

    def login(self):
        url = "https://worktime.evolable.asia/"
        r = requests.get(url)
        self.cookies = r.cookies
        data = {
            "_method": "POST",
            "data[EwtUser][email_login]": self.email,
            "data[EwtUser][password_login]": self.password,
            "data[EwtUser][captcha]": self.get_captcha()
        }

        r = requests.post(
            "https://worktime.evolable.asia/ewt_users/login",
            headers=self._default_headers(),
            data=data)
        print(data)
        print(r.content.decode("UTF-8"))

    def get_captcha(self):
        self._save_captcha()
        return Captcha.translate("c.png")

    def _save_captcha(self):
        captcha_url = "https://worktime.evolable.asia/ewt_users/captcha_image/"
        r = requests.get(captcha_url, headers=self._default_headers())

        f = open("../files/c.png", "wb")
        f.write(r.content)

    def _default_headers(self):
        return {
            "Accept":
            "image/webp,image/apng,image/*,*/*;q=0.8",
            "Accept-Encoding":
            "gzip,deflate,br",
            "Accept-Language":
            "en-US,en;q=0.8",
            "Connection":
            "keep-alive",
            "Cookie":
            "PHPSESSID=%s" % self.cookies["PHPSESSID"],
            "Host":
            "worktime.evolable.asia",
            "Referer":
            "https://worktime.evolable.asia/",
            "User-Agent":
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like"
            " Gecko) Chrome/61.0.3163.100 Safari/537.36"
        }


w = WorkTime("tienhm@evolableasia.vn", "test_password")
w.login()
