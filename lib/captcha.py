import subprocess


class Captcha(object):
    """Resolve worktime captcha by tesseract"""
    @classmethod
    def translate(cls, filename):
        """
        Translate captcha image to string
        :param filename: name of captcha file
        """
        filename = "../files/" + filename
        cls._convert_image(filename)
        return cls._tesseract(filename)

    @staticmethod
    def _convert_image(filename):
        """
        Convert captcha image to readable for Tesseract
        :param filename: name of file to convert
        """
        return subprocess.run([
            "convert",
            filename,
            "-colorspace", "gray",
            "-threshold", "40%",
            filename
        ])

    @staticmethod
    def _tesseract(filename):
        """
        Read captcha
        :param filename: name of captcha file
        """
        result = subprocess.run([
            "tesseract",
            "-psm", "8",
            filename, "-"
        ], stdout=subprocess.PIPE)
        return result.stdout.replace(b'\n\n', b'').decode("UTF-8")
